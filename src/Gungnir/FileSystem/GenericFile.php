<?php
namespace Gungnir\Core\FileSystem;

class GenericFile implements File
{
    private $extension = null;
    private $content = null;

    /**
     * Get file extension of this file
     */
    public function getExtension() : String
    {
        return $this->extension;
    }

    /**
     * Set file extension of this file
     */
    public function setExtension(String $extension)
    {
        $this->extension = $extension;
        return $this;
    }

    /**
     * Get content of this file
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content of this file
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }
}
