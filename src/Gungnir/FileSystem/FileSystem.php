<?php
namespace Gungnir\Core\FileSystem;

class FileSystem
{
    private $root = null;

    /**
     * Constructor
     *
     * @param String $root The path to project root folder
     */
    public function __construct(String $root)
    {
        $this->root = $root;
    }

    public function openFile() : File
    {
        return new GenericFile;
    }
}
