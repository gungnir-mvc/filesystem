<?php
namespace Gungnir\Core\FileSystem;

interface File
{
    /**
     * Get file extension of this file
     */
    public function getExtension() : String;

    /**
     * Set file extension of this file
     */
    public function setExtension(String $extension);

    /**
     * Get content of this file
     */
    public function getContent();

    /**
     * Set content of this file
     */
    public function setContent($content);
}
